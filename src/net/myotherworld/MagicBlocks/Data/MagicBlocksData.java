package net.myotherworld.MagicBlocks.Data;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.myotherworld.MagicBlocks.MagicBlocks;

public class MagicBlocksData 
{
	private YamlConfiguration config;
	private File configFile;
	private MagicBlocks plugin;
	
	public List<String> enabledWorlds = new ArrayList<String>();
	public String Permissions;
	public String Prefix;
	public String MagicBlocksOn;
	public String MagicBlocksOff;
	
	public MagicBlocksData(MagicBlocks pl)
	{
		this.plugin = pl;
		
		reloadData();
	}
	
	public void reloadData()
	{
		load();
		this.enabledWorlds.clear();
		this.enabledWorlds = this.config.getStringList("config.worlds");	
		
		this.Prefix = ChatColor.translateAlternateColorCodes('&',this.config.getString("Prefix", "MagicBlocks"));	
		
		this.Permissions = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.Permissions", "You do not have permission!" ));
		this.MagicBlocksOn = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.MagicBlocksOn", "MagicBlock On" ));
		this.MagicBlocksOff = ChatColor.translateAlternateColorCodes('&',this.config.getString("Message.MagicBlocksOff", "MagicBlock Off" ));
	}
	
	public void load()
	{
		try
		{
			if (!this.plugin.getDataFolder().exists()) 
			{
				this.plugin.getDataFolder().mkdirs();
			}
			if (!new File(this.plugin.getDataFolder(), "config.yml").exists()) 
			{
				this.plugin.saveResource("config.yml", false);
			}
			this.configFile = new File(this.plugin.getDataFolder(), "config.yml");
			this.config = YamlConfiguration.loadConfiguration(this.configFile);
		}
		catch (Exception ex)
		{
			this.plugin.getLogger().severe("Blad podczas wczytywania konfiguracji!");
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
			return;
		}
	}
	public void save(String patch, Object value)
	{
		this.config.set(patch, value);
		try
		{
			this.config.save(this.configFile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	
}

