package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SecretBlocksManger
{
	public static void SecretWool(Player player, Location loc, Material mat)
	{
	    loc.setY(loc.getY() - 1.0D);   
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {	
	    	if(loc.getBlock().getType() == Material.WOOL)
	    	{
	    		Block block = loc.getBlock();
	    		if ((byte)block.getData() == 1)
	    		{
		    		//Blindness V 3 sec
		    		player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS,80,4));
	    		}
	    		if ((byte)block.getData() == 2)
	    		{
		    		//Czysczenie efektow
		    		for (PotionEffect effect : player.getActivePotionEffects())
		            player.removePotionEffect(effect.getType());
	    		}
	    		if ((byte)block.getData() == 3)
	    		{
	    			//Confusion II 10 sec
		    		player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION,200,1));
	    		}
	    		if ((byte)block.getData() == 4)
	    		{
	    			//Height 1lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(0.5D).setY(1.0D));    		
	    		}
	    		//if ((byte)block.getData() == 5)
	    		//{
	    			//Height 2lvl
	    			//player.setVelocity(player.getLocation().getDirection().multiply(0.5D).setY(1.5D));    		
	    		//}
	    		if ((byte)block.getData() == 6)
	    		{
	    			//Height 3lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(0.5D).setY(2.0D));    		
	    		}
	    		if ((byte)block.getData() == 7)
	    		{
	    			//Jump 1lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(1.0D).setY(1.0D));    		
	    		}
	    		if ((byte)block.getData() == 8)
	    		{
	    			//Jump 2lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(1.5D).setY(1.5D));    		
	    		}
	    		if ((byte)block.getData() == 9)
	    		{
	    			//Jump 3lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(2.0D).setY(2.0D));    		
	    		}	
	    		if ((byte)block.getData() == 10)
	    		{
	    			//Length 1lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(1.0D).setY(0.5D));    		
	    		}		    		
	    		if ((byte)block.getData() == 10)
	    		{
	    			//Length 2lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(1.5D).setY(0.5D));    		
	    		}	
	    		if ((byte)block.getData() == 11)
	    		{
	    			//Length 3lvl
	    			player.setVelocity(player.getLocation().getDirection().multiply(2.0D).setY(0.5D));    		
	    		}
	    		if ((byte)block.getData() == 12)
	    		{
	    			//Slow IV 60 sec
		    		player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,1200,3));
	    		}
	    		if ((byte)block.getData() == 13)
	    		{
	    			//Speed II 3 sec
		    		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,60,1));	    
	    		}
	    		//if ((byte)block.getData() == 14)
	    		//{
	    			//Speed II 6 sec
		    	//	player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,120,1));	    
	    		//}
	    		if ((byte)block.getData() == 15)
	    		{
		    		//Speed II 9 sec
		    		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,180,1));	    
	    		}	
	    	}
	    }
	}
}