package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundBlocksManager
{
	public static void NoteBlockSound(Player player, Location loc, Material mat)
	{
		if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
		{	
			if(loc.getBlock().getType() == Material.NOTE_BLOCK)
			{	
				player.playSound(player.getLocation(), Sound.EXPLODE, 100, 0 );   
			}
		}
	}
}
