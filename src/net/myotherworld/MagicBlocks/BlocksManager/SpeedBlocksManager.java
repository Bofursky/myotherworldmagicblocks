package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpeedBlocksManager
{
	public static void OreSpeed(Player player, Location loc, Material mat)
	{
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {	 
	    	loc.setY(loc.getY() + 1.0D); 
	    	//1 Poziom
	    	if(loc.getBlock().getType() == Material.LAPIS_BLOCK)
	    	{	
	    		//Speed II 3 sec
	    		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,60,1));	    
	    	}
	    	//2 Poziom
	    	else if(loc.getBlock().getType() == Material.DIAMOND_BLOCK)
	   		{
	    		//Speed II 6 sec
	    		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,120,1));
	   		}
	    	//3 Poziom
	    	else if(loc.getBlock().getType() == Material.EMERALD_BLOCK)
	    	{
	    		//Speed II 9 sec
	    		player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,180,1));
	    	}
	    }
	}
}
