package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class HeightBlocksManager
{
	public static void HightBlock(Player player, Location loc, Material mat)
	{
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {	
	    	//1 Poziom	    
	    	if(loc.getBlock().getType() == Material.COAL_BLOCK)
	    	{
	    		player.setVelocity(player.getLocation().getDirection().multiply(0.5D).setY(1.0D));    		
	    	}	   		    	
	    	//2 Poziom
	    	else if(loc.getBlock().getType() == Material.IRON_BLOCK)
	    	{
	    		player.setVelocity(player.getLocation().getDirection().multiply(0.5D).setY(1.5D));	    
	    	}
	    	//3 Poziom
	    	else if(loc.getBlock().getType() == Material.GOLD_BLOCK)
	    	{
	    		player.setVelocity(player.getLocation().getDirection().multiply(0.5D).setY(2.0D));
	    
	    	}
	    }

	}
}
