package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class LengthBlocksManager
{
	public static void OreLength(Player player, Location loc, Material mat)
	{
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {	
	    	//1 Poziom
	    	if(loc.getBlock().getType() == Material.COAL_ORE)
	    	{
	    		player.setVelocity(player.getLocation().getDirection().multiply(1.0D).setY(0.5D));
	    	}
	    	//2 Poziom
	    	else if(loc.getBlock().getType() == Material.IRON_ORE)
	    	{
	    		player.setVelocity(player.getLocation().getDirection().multiply(1.5D).setY(0.5D));
	    	} 
	    	//3 Poziom
	    	else if(loc.getBlock().getType() == Material.GOLD_ORE)
	    	{
	    		player.setVelocity(player.getLocation().getDirection().multiply(2.0D).setY(0.5D));
	    
	    	}
	    }
	}

}
