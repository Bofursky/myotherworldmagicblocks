package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class ClearBlocksManager
{
	public static void SpongeClear(Player player, Location loc, Material mat)
	{
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {		    
	    	if(loc.getBlock().getType() == Material.SPONGE)
	    	{	
	    		//Czysczenie efektow
	    		for (PotionEffect effect : player.getActivePotionEffects())
	            player.removePotionEffect(effect.getType());
	    	}
	    
	    }
	}
}
