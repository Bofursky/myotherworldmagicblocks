package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class SecretSoundBlocksManager 
{
	public static void SecretSoundBlocks(Player player, Location loc, Material mat)
	{
		//loc.setY(loc.getY() - 1.0D);   
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {
	    	if(loc.getBlock().getType() == Material.LOG)
	    	{
	    		Block block = loc.getBlock();
	    		if ((byte)block.getData() == 1)
	    		{
	    			player.playSound(player.getLocation(), Sound.EXPLODE, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 2)
	    		{
	    			player.playSound(player.getLocation(), Sound.GHAST_SCREAM, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 3)
	    		{
	    			player.playSound(player.getLocation(), Sound.CREEPER_HISS, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 4)
	    		{
	    			player.playSound(player.getLocation(), Sound.DONKEY_IDLE, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 5)
	    		{
	    			player.playSound(player.getLocation(), Sound.IRONGOLEM_HIT, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 6)
	    		{
	    			player.playSound(player.getLocation(), Sound.BLAZE_DEATH, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 7)
	    		{
	    			player.playSound(player.getLocation(), Sound.CREEPER_DEATH, 100, 0 );
	    		}	
	    		if ((byte)block.getData() == 8)
	    		{
	    			player.playSound(player.getLocation(), Sound.FIREWORK_LARGE_BLAST2, 100, 0 );
	    		}	
	    		if ((byte)block.getData() == 9)
	    		{
	    			player.playSound(player.getLocation(), Sound.WOLF_DEATH, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 10)
	    		{
	    			player.playSound(player.getLocation(), Sound.FIREWORK_LAUNCH, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 11)
	    		{
	    			player.playSound(player.getLocation(), Sound.WOLF_GROWL, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 12)
	    		{
	    			player.playSound(player.getLocation(), Sound.CAT_MEOW, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 13)
	    		{
	    			player.playSound(player.getLocation(), Sound.ENDERDRAGON_GROWL, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 14)
	    		{
	    			player.playSound(player.getLocation(), Sound.ENDERDRAGON_HIT, 100, 0 );
	    		}
	    		if ((byte)block.getData() == 15)
	    		{
	    			player.playSound(player.getLocation(), Sound.COW_HURT, 100, 0 );
	    		}
			}	    	
	    }
	}
}

