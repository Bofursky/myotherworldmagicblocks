package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SlowBlocksManager
{
	public static void PackedIceSlow(Player player, Location loc, Material mat)
	{
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {	
	    	//loc.setY(loc.getY() + 1.0D); 
	    	if(loc.getBlock().getType() == Material.PACKED_ICE)
	    	{	
	    		//Slow IV 60 sec
	    		player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,1200,3));
	    
	    	}
	    }
	}
}
