package net.myotherworld.MagicBlocks.BlocksManager;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class BlindnessBlocksManager
{
	
	public static void GlowstoneBlindness(Player player, Location loc, Material mat)
	{  
	    if (MagicBlocks.data.enabledWorlds.contains(player.getWorld().getName()))
	    {	
	    	if(loc.getBlock().getType() == Material.GLOWSTONE)
	    	{	
	    		//Blindness V 3 sec
	    		player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS,80,4));
	    	}
	    }
	}
}
