package net.myotherworld.MagicBlocks;


import net.myotherworld.MagicBlocks.Command.ReloadCommand;
import net.myotherworld.MagicBlocks.Command.ToggleCommand;
import net.myotherworld.MagicBlocks.Data.MagicBlocksData;
import net.myotherworld.MagicBlocks.Listener.ClearEffectsListener;
import net.myotherworld.MagicBlocks.Listener.OnFallListener;
import net.myotherworld.MagicBlocks.Listener.PlayerMoveEventListener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class MagicBlocks extends JavaPlugin
{
	public static MagicBlocksData data;
	
    private void listeners()
    {
    	PluginManager pm = getServer().getPluginManager();
    	pm.registerEvents(new OnFallListener(), this);
        pm.registerEvents(new PlayerMoveEventListener(), this);     
        pm.registerEvents(new ClearEffectsListener(), this);   
    }

    
	@Override
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEnable() 
	{  
		data = new MagicBlocksData(this);
		listeners();
		
		getCommand("Magicreload").setExecutor( new ReloadCommand() );
		getCommand("Toggle").setExecutor( new ToggleCommand() );
	}
}