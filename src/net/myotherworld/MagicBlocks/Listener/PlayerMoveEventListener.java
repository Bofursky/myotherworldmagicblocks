package net.myotherworld.MagicBlocks.Listener;

import net.myotherworld.MagicBlocks.BlocksManager.BlindnessBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.ClearBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.ConfusionBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.HeightBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.JumpBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.LengthBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.NoJumpBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.PistonBlocksManger;
import net.myotherworld.MagicBlocks.BlocksManager.SecretBlocksManger;
import net.myotherworld.MagicBlocks.BlocksManager.SecretSoundBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.SlowBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.SoundBlocksManager;
import net.myotherworld.MagicBlocks.BlocksManager.SpeedBlocksManager;
import net.myotherworld.MagicBlocks.Command.ToggleCommand;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveEventListener implements Listener
{

	@EventHandler
	public void PlayerMoveEvent(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		Location loc = player.getLocation();
		Material mat = loc.getBlock().getType();
		loc.setY(loc.getY() - 1.0D);  

	    	if(!ToggleCommand.Events.contains(player))
	    	{
		    	ClearBlocksManager.SpongeClear(player, loc, mat);
		    	BlindnessBlocksManager.GlowstoneBlindness(player , loc, mat);
				ConfusionBlocksManager.JackOLanternConfusion(player, loc, mat);
				HeightBlocksManager.HightBlock(player, loc, mat);
				JumpBlocksManager.OreJump(player, loc, mat);
				LengthBlocksManager.OreLength(player, loc, mat);
				PistonBlocksManger.PistonDirection(player, loc, mat);
				SecretBlocksManger.SecretWool(player, loc, mat);
				SpeedBlocksManager.OreSpeed(player, loc, mat);
				SlowBlocksManager.PackedIceSlow(player, loc, mat);
				SoundBlocksManager.NoteBlockSound(player, loc, mat);
				NoJumpBlocksManager.EnderStoneNoJump(player, loc, mat);
				SecretBlocksManger.SecretWool(player, loc, mat);
				SecretSoundBlocksManager.SecretSoundBlocks(player, loc, mat);
	    	}
	    
		
	}
}
