package net.myotherworld.MagicBlocks.Listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

public class ClearEffectsListener implements Listener 
{
	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event)
	{    
		Player player = event.getPlayer();
		for (PotionEffect effect : player.getActivePotionEffects())
			player.removePotionEffect(effect.getType());    		   	    
	}
	@EventHandler
	public void QuitSever(PlayerQuitEvent event)
	{    
		Player player = event.getPlayer();
		for (PotionEffect effect : player.getActivePotionEffects())
			player.removePotionEffect(effect.getType());    		   	    
	}
}
