package net.myotherworld.MagicBlocks.Command;

import java.util.ArrayList;

import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class ToggleCommand implements Listener, CommandExecutor
{
	public static ArrayList<Player> Events = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if (!(sender instanceof Player)) 
		{
			sender.sendMessage("Tylko gracz moze tego uzyc!");
			return true;
	    }
		if(sender.hasPermission("MyOtherWorldMagicBlocks.toggle")) 
		{
			Player player = (Player)sender;
			if (!Events.contains(player))
			{
				Events.add(player);
				player.sendMessage(MagicBlocks.data.Prefix + MagicBlocks.data.MagicBlocksOff);
				return true;
			}
			else 
			{		
				Events.remove(player);
				player.sendMessage(MagicBlocks.data.Prefix + MagicBlocks.data.MagicBlocksOn);
				return true;
			}
		}		
		else
		{
			sender.sendMessage(MagicBlocks.data.Prefix + MagicBlocks.data.Permissions);
		}
		return true;	     
	}
	

}
