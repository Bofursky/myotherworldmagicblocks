package net.myotherworld.MagicBlocks.Command;


import net.myotherworld.MagicBlocks.MagicBlocks;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandExecutor 
{
	public boolean onCommand(CommandSender sender, Command cmd,String label, String[] args) 
	{
		if(sender.hasPermission("MyOtherWorldMagicBlocks.essreload")) 
		{		
	        MagicBlocks.data.reloadData();
	        sender.sendMessage(MagicBlocks.data.Prefix + "Poprawnie przeladowano config.");
	        return true;
		}
		else
		{
			sender.sendMessage(MagicBlocks.data.Prefix + MagicBlocks.data.Permissions);
		}
		return true;	     
	}
	

}
